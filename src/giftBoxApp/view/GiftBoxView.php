<?php
namespace giftBoxApp\view;

class GiftBoxView extends \mf\view\AbstractView {
  
    public function __construct( $data ){
        parent::__construct($data);
    }


    private function renderHeader(){
        return '<h1>GiftBoxApp</h1>';
    }
    

    private function renderFooter(){
        return 'La super app créée en Licence Pro &copy;2018';
    }

    
    private function renderPrestations(){

        $router = new \mf\router\Router(); 
        $app_root = (new \mf\utils\HttpRequest())->root;       

        $res = "<article class='row'>";
        $res .= "<h2 class='s12 m12 l12 tListPresta'>Liste des prestations</h2>"; 

        $res .= "<section class='col s12 m3 l2 row'>";
        $res .= "<ul class='cat col s12 m12 l12 row'>";

        foreach($this->data['categories'] as $key => $t){

            $res .= "<li class='col s3 m12 l12'><a href='".$router->urlFor('categorie',['id' => $t->id])."'>$t->nom</a></li>";
        }



        $res .= "</ul>       
        </section>
        <section class='col s1 m1 l2 row'>
         <img class='col s2 m1 l3 offset-l2 linepen' src='$app_root/html/images/linepen.jpg'>
         </section>'";



        $res .= "<section class='col s12 m9 l8 row'>";

        foreach($this->data['prestations'] as $key => $t){

            $res .= "<div class='col s12 m6 l4 prestation'>";
            $res .= "<div class='row pres_info'>";
            $res .= "<span class='col s10 m10 l10'>$t->nom</span>";
            $res .= "<p class='col s2 m2 l2'>$t->prix</p>";
            $res .= "</div>";
            $res .= "<img class='imgPres' src='$app_root/html/images/$t->img' />";
            $res .= "</div>";
        }

        $res .= "</section>";
        $res .= "</article>";
        
        return $res;
    }

    private function renderCategoriePrestations(){
        $router = new \mf\router\Router(); 
        $app_root = (new \mf\utils\HttpRequest())->root; 

        
        
        if(!is_null($this->data['categorie'])){
            $res = "<article class='row'>";
            $res .= "<h2 class='col s12 m12 l12'>".$this->data['categorie']->nom."</h2>";

            $res .= "<a class='col offset-s3 offset-m3 offset-l3 s2 m2 l2' href='".$router->urlFor('categorie',['id' => $this->data['categorie']->id,'tri' => 'DESC'])."'>DESC</a>";
            $res .= "<a class='col offset-s3 offset-m3 offset-l3 s2 m2 l2' href='".$router->urlFor('categorie',['id' => $this->data['categorie']->id,'tri' => 'ASC'])."'>ASC</a>";

            $res .= "<section class='col s12 m3 l3 row'>";
            $res .= "<ul class='cat col s12 m12 l12 row'>";

            foreach($this->data['categories'] as $key => $t){

                $res .= "<li class='";
                    if($t->id == $this->data['categorie']->id)
                    $res .= "active";
                $res .= " col s3 m12 l12'><a href='".$router->urlFor('categorie',['id' => $t->id])."'>$t->nom</a></li>";
            }

            $res .= "</ul></section>";

            $res .= "<section class='col s12 m9 l9 row'>";

                foreach($this->data['prestations'] as $key => $t){

                    $res .= "<div class='col s12 m6 l4 prestation'>";
                    $res .= "<div class='row pres_info'>";
                    $res .= "<span class='col s10 m10 l10'>$t->nom</span>";
                    $res .= "<p class='col s2 m2 l2'>$t->prix</p>";
                    $res .= "</div>";
                    $res .= "<a href='".$router->urlFor('prestation',['id' => $t->id])."'>";
                    $res .= "<img class='imgPres' src='$app_root/html/images/$t->img' /></a>";
                    $res .= "</div>";
                }
        
                $res .= "</section>";
                $res .= "</article>";

            return $res;
            }else{
                return;
            }
    }

    private function renderPrestation(){

        $router = new \mf\router\Router(); 
        $app_root = (new \mf\utils\HttpRequest())->root; 

         if(!is_null($this->data['prestation'])){
            
            $res = "<article class=''>";
            $res .= "<div class=''>"; 
                    $res .= "<p>".$this->data['prestation']->nom."</p>";
                    $res .= "<p>".$this->data['prestation']->prix."</p>";
                    $res .= "<span>Categorie :<a href='".$router->urlFor('categorie',['id' => $this->data['categorie']->id])."' class=''>".$this->data['categorie']->nom."</a></span>";
                    $res .= "<p>".$this->data['prestation']->description."</p>";
                    $res .= "<img class='imgPres' src='$app_root/html/images/".$this->data['prestation']->img."' />";
            $res .= "</div>";
            $res .= "</article>";
                
            return $res;
         }else{
             return;
         }

    }
  
     
    

    private function renderViewSignup(){
        $router = new \mf\router\Router();

        $res = "<form id='tweet-form' class='forms' method='POST' action='".$router->urlFor('checksignup')."'>".       
        "<input class='input' type='text' name='fullname' id='fullname' placeholder='full name' required /><br>".
        "<input class='input' type='test' name='username' id='username' placeholder='username' required /><br>".
        "<input class='input' type='password' name='password' id='password' placeholder='password' required /><br>".
        "<input class='input' type='password' name='rpassword' id='rpassword' placeholder='retype password' required /><br>".  
        "<input class='button input' type='submit' value='Sign up' id='signup' name='signup' /></form>";

        return $res;
    }

    private function renderViewLogin(){
        $router = new \mf\router\Router();
        
        $res = "<form id='tweet-form' class='forms' method='POST' action='".$router->urlFor('checkLogin')."'> ".       
        "<input class='input' type='test' name='username' id='username' placeholder='username' required /><br>".
        "<input class='input' type='password' name='password' id='password' placeholder='password' required /><br>".  
        "<input class='button input' type='submit' value='Send' id='' name='send' /></form>";

        return $res;
    }

    // private function renderViewFormulaire(){
    //     $router = new \mf\router\Router();
        
    //     $res = "<form id='tweet-form' class='forms' method='POST' action='".$router->urlFor('send')."'> ".       
    //     "<textarea name='textarea' id='textarea' placeholder='Enter tweet...'></textarea><br>".  
    //     "<input class='button input' type='submit' value='Send' id='' name='send' /></form>";

    //     return $res;
    // }

    private function renderBottomMenu(){
        $router = new \mf\router\Router();
        $res = "<div class=''>";
        // $res = "<a href='".$router->urlFor('post')."' class=''>new tweet</a>";
        $res .= "</div>";
        return $res;
    }

    public function renderCagnotte(){

        $router = new \mf\router\Router();

        $resultatAddCagnotte = "<form action='".$router->urlFor('addCagnotte')."' method='post'> ".
        "<label>Prix </label>".
        "<input class='input' type='text' name='price' id='prices' placeholder='Prix' required /><br>".
        "<label>Nom </label>".
        "<input class='input' type='text' name='fullname' id='fullname' placeholder='full name' required /><br>".
        "<label>Prenom </label>".
        "<input class='input' type='text' name='lastname' id='lastname' placeholder='last name' required /><br>".
        "<label>Message </label>".
        "<input class='input' type='text' name='message' id='message' placeholder='message' required /><br>".
        "<input class='button input' type='submit' value='Creer votre cagnotte' id='addCagnotte' name='addCagnotte' /></form>";

        return $resultatAddCagnotte;
    }

    //  public function renderCoffret(){

    //     $router = new \mf\router\Router();

    //     $resultatAddCoffret = "<form action='".$router->urlFor('addCoffret')."' method='post'> ".
    //     "<label>Nom </label>".
    //     "<input class='input' type='text' name='fullname' id='fullname' placeholder='full name' required /><br>".
    //      "<input class='button input' type='submit' value='Creer votre coffret' id='addCoffret' name='addCoffret' /></form>";
    //     return $resultatAddCoffret;
    // }

    private function renderTopMenu(){
        $router = new \mf\router\Router();
        // $http_req = new \mf\utils\HttpRequest();
        $res = "";
        // $res .= "<div class='row'><img class='col s12 m12 l6    ' src='html/images/bancadeau.jpg'></div>'";
         $res .= "<nav id='nav-menu'>";
         $res .="<section class='col s12 m12 l12 col'>";
         $res .= "<a class='button col s3 m2 l2' href='".$router->urlFor('prestations')."'>ACCUEIL</a>";
         $res .= "<a class='button col s3 m2 l2' href='".$router->urlFor('coffrets')."'>COFFRETS</a>";
         $res .= "<a class='button col s3 m2 l2' href='".$router->urlFor('prestations')."'>PRESTATIONS</a>";
         if(true){
            // $res .= "<a class='button col s2 m2 l' href='$http_req->script_name'>PANEL ADMIN</a>";
         }        
        if(isset($_SESSION['user_login'])){
            // $res .= "<a class='button' href='".$router->urlFor('followers')."'>followers</a>";
            $res .= "<a class='button' href='".$router->urlFor('logout')."'> LOGOUT </a>";
        }else{
            $res .= "<a class='button button col s3 m2 l2' href='".$router->urlFor('login')."'> LOGIN </a>";
            $res .= "<a class='button' href='".$router->urlFor('signup')."'> SIGNUP </a>";
        }
        $res .= "<a class='button col s2 m2 l2 offset-l6' href=''>?GiftBoxApp?</a>";
        
        return $res;
        
    }

    private function renderCoffrets(){
        $router = new \mf\router\Router();
        // $http_req = new \mf\utils\HttpRequest();        
        $res = "";
        var_dump($this->data);
        foreach ($this->data as $unCoffret) {
            $res .= "Nom du coffret :<a href='".$router->urlFor('coffret')."?id=".$unCoffret->id."'> $unCoffret->nom</a><br>";
            $res .= "Prix : $unCoffret->prix"; 
        }
        
        return $res;
    }

    private function renderCoffretDetail(){
        $router = new \mf\router\Router();
        // $http_req = new \mf\utils\HttpRequest();
        var_dump($this->data);       
        $res = "";       
        foreach ($this->data as $unCoffret) {
            $res .= "Nom du coffret : $unCoffret->nom</a><br>";
            $res .= "Prix : $unCoffret->prix";
        }
        
        return $res;
    }

    
    protected function renderBody($selector=null){

        /*
         * voire la classe AbstractView
         * 
         */
         $http_req = new \mf\utils\HttpRequest();

         $res = "";



         


         $res .= "</nav></section></header>";

         $res .= "<header class='theme-backcolor1'>".$this->renderHeader();
         
         //--------test login---------
         if(isset($_SESSION['user_login'])){
            $res .= "username : ".$_SESSION['user_login'];
            $res .= "<br>access_level : ".$_SESSION['access_level'];
         }
         //----------------------------
         
         $res .= "<nav id='nav-menu'>";

         $res .= $this->renderTopMenu();

         $res .= "</nav></header>";

         $res .= "<section>";

         if($selector == 'prestations')
         $res .= "<section>".$this->renderPrestations()."</section>";

         if($selector == 'categoriePrestations')
         $res .= "<section>".$this->renderCategoriePrestations()."</section>";

         if($selector == 'prestation')
         $res .= "<section>".$this->renderPrestation()."</section>";

         if($selector == 'login')
         $res .= $this->renderViewLogin();
       
         if($selector == 'signup')
         $res .= $this->renderViewSignup();



        switch ($selector) {
            case 'coffrets':
                $res .= "<section>".$this->renderCoffrets()."</section>";
                break;
            case 'coffretDetail':
                $res .= "<section>".$this->renderCoffretDetail()."</section>";
                break;
        }

        if(isset($_SESSION['user_login']))
         $res .= $this->renderBottomMenu();

         $res .= "</section>";

         $res .= "<footer class='theme-backcolor1'>".$this->renderFooter()."</footer>";

         return $res;
    }











    
}

?>