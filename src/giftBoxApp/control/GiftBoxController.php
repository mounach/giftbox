<?php
namespace giftBoxApp\control;

use giftBoxApp\model\Prestation;
use giftBoxApp\model\Categorie;
use giftBoxApp\model\Coffret;
use giftBoxApp\model\User;

class GiftBoxController extends \mf\control\AbstractController {

    
    public function __construct(){
        parent::__construct();
    }

    
    public function viewPrestations(){

        if(isset($_GET['tri']))
            $prestations = Prestation::select()->orderBy('prix', $_GET['tri'])->get();
        else {
            $prestations = Prestation::select()->get();
        }
         $categories = Categorie::select()->get();
        
         $vue = new \giftBoxApp\view\GiftBoxView([
            'categories' => $categories,
            'prestations' => $prestations
            ]);

         return $vue->render('prestations');

    }

    public function viewCoffrets(){
        if(isset($_SESSION['user_login'])){           
            $user = User::select()->where('username','=',$_SESSION['user_login'])->first();
            $coffrets = $user->coffrets()->get();
            
             // $coffrets = Coffret::select()->where('id_user','=',$user->id)->get();
             var_dump($coffrets);
            $vue = new \giftBoxApp\view\GiftBoxView($coffrets);
            return $vue->render('coffrets');
        }else{
            echo 'noooooow god !';
        }
           
    }

    public function viewCoffretDetail(){
        if (isset($_GET['id'])) {
            $coffret = Coffret::where('id','=',$_GET['id']);
            $vue = new \giftBoxApp\view\GiftBoxView($coffret);            
            return $vue->render('coffretDetail');
        }

    }






    public function viewCategoriePrestations(){

        if(isset($_GET['id'])){

            $cat_id = $_GET['id'];
            $categories = Categorie::select()->get();
            $categorie = Categorie::where('id','=',$cat_id)->first();
            if(!is_null($categorie)){
                if(isset($_GET['tri']))
                    $prestations = $categorie->prestations()->orderBy('prix', $_GET['tri'])->get();
                else {
                    $prestations = $categorie->prestations()->get();
                }
            }
            else{
             $prestations = null;
            }
 
            $vue = new \giftBoxApp\view\GiftBoxView([
                'categorie' => $categorie,
                'categories' => $categories,
                'prestations' => $prestations
                ]);
 
            return $vue->render('categoriePrestations');  
 
         }else{

         }

    }
    
    public function viewCagnotte() {

            $vue = new \giftBoxApp\view\GiftBoxView('');
            return $vue->render('cagnotte');
        }


        public function viewAddCagnotte(){

               if (isset($_POST['addCagnotte'])) {
                $cagnotte = new \giftBoxApp\model\Cagnotte;
                $cagnotte->prix = $_POST['price'];
                $cagnotte->nom = $_POST['fullname'];
                $cagnotte->prenom = $_POST['lastname'];
                $cagnotte->message = $_POST['message'];
                $cagnotte->save();}

                $vue = new \giftBoxApp\view\GiftBoxView();

        }




        public function viewAddPrestationCoffret(){

               if (isset($_GET['id'])) {

                $coffret = new \giftBoxApp\model\Coffret;
                $coffret->nom = $_POST['fullname'];
                $coffret->save();}
                $vue = new \giftBoxApp\view\GiftBoxView();
        }


        public function viewAddCoffret(){

               if (isset($_POST['addCoffret'])) {
                $coffret = new \giftBoxApp\model\Coffret;
                $coffret->nom = $_POST['fullname'];
                $coffret->save();}
                $vue = new \giftBoxApp\view\GiftBoxView();
        }

    public function ViewPrestation(){

        $res = "";

         if(isset($_GET['id'])){

            $prestation_id = $_GET['id'];

            $prestation = Prestation::where('id','=',$prestation_id)->first();
            if(!is_null($prestation))
                $categorie  = $prestation->categorie()->first();

                $vue = new \giftBoxApp\view\GiftBoxView([
                    'categorie' => $categorie,
                    'prestation' => $prestation
                    ]);
            
            

            return $vue->render('prestation');

         }

    }


}