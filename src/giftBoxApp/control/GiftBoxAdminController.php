<?php
namespace giftBoxApp\control;

use giftBoxApp\model\User;
use giftBoxApp\auth\GiftBoxAuthentification;
use mf\router\Router;

class GiftBoxAdminController extends \mf\control\AbstractController {


    public function __construct(){
        parent::__construct();
    }

    //viewFormulaireLogin
    public function viewLogin(){
        $vue = new \giftBoxApp\view\GiftBoxView('');

        return $vue->render('login');
    }

    public function logout(){
        
        $giftBoxAuth = new GiftBoxAuthentification();

        $giftBoxAuth->logout();

        $route = new Router();
        $route->executeRoute('default');
        
    }

    //checkLogin
    public function checkLogin(){

        if(isset($_POST['username'])){
            
            $giftBoxAuth = new GiftBoxAuthentification();
            $route = new Router();

            if($giftBoxAuth->loginUser($_POST['username'], $_POST['password'])){
 
                     $route->executeRoute('default');

            }else{
                $route->executeRoute('default');
            }
        }
    }

    public function signup(){
        
        $vue = new \giftBoxApp\view\GiftBoxView('');

        return $vue->render('signup');
        
    }


    public function checksignup(){
        
            $giftBoxAuth = new GiftBoxAuthentification();
            $route = new Router();
                
        if(isset($_POST['username'])){
            if($_POST['password'] == $_POST['rpassword']){
                $username = filter_input(INPUT_POST, "username", FILTER_DEFAULT);
                $password = filter_input(INPUT_POST, "password", FILTER_DEFAULT);
                $fullname = filter_input(INPUT_POST, "fullname", FILTER_DEFAULT);
                if($giftBoxAuth->createUser($username, $password, $fullname)){
                    $route->executeRoute('login');
                }else{
                    $route->executeRoute('signup');
                }
            }else{
                $route->executeRoute('signup');
            }
                
        }
        
        
    }


    //--------------------

    //viewFormulaire
    public function viewCreatePrestation(){
        $vue = new \giftBoxApp\view\GiftBoxView('');

        return $vue->render('createPrestation');
    }

        //createNewPrestation
    public function createPrestation(){
        
        $prestation = new Prestation();

        if(isset($_SESSION['user_login'])){
            
            if(isset($_POST['categorie'])){
                // $tweet->text = filter_input(INPUT_POST, "textarea", FILTER_DEFAULT);
                // $tweet->author = $user->id;

                // $tweet->save();

                $route = new Router();
                $route->executeRoute('default');
            }
        }
        
    }

    //--------------------
}


?>