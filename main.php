<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
/* pour le chargement automatique des classes dans vendor */
require_once 'vendor/autoload.php';

require_once 'src/mf/utils/ClassLoader.php';

$loader = new mf\utils\ClassLoader("src");
$loader->register();

use mf\auth\Authentification;
use giftBoxApp\auth\GiftBoxAuthentification;
use mf\router\Router;







$init = parse_ini_file("conf/config.ini");

$config = [
    'driver'    => $init["type"],
    'host'      => $init["host"],
    'database'  => $init["name"],
    'username'  => $init["user"],
    'password'  => $init["pass"],
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '' ];

/* une instance de connexion  */
$db = new Illuminate\Database\Capsule\Manager();

$db->addConnection( $config ); /* configuration avec nos paramètres */
$db->setAsGlobal();            /* visible de tout fichier */
$db->bootEloquent();           /* établir la connexion */

session_start();

$router = new \mf\router\Router();

$router->addRoute('prestations', '/prestations/', '\giftBoxApp\control\GiftBoxController', 'viewPrestations',GiftBoxAuthentification::ACCESS_LEVEL_NONE);
$router->addRoute('categorie', '/categorie/', '\giftBoxApp\control\GiftBoxController', 'viewCategoriePrestations',GiftBoxAuthentification::ACCESS_LEVEL_NONE);
$router->addRoute('prestation', '/prestation/', '\giftBoxApp\control\GiftBoxController', 'viewPrestation',GiftBoxAuthentification::ACCESS_LEVEL_NONE);

$router->addRoute('login', '/login/', '\giftBoxApp\control\GiftBoxAdminController', 'viewLogin',GiftBoxAuthentification::ACCESS_LEVEL_NONE);
$router->addRoute('checkLogin', '/checkLogin/', '\giftBoxApp\control\GiftBoxAdminController', 'checkLogin',GiftBoxAuthentification::ACCESS_LEVEL_NONE);
$router->addRoute('logout', '/logout/', '\giftBoxApp\control\GiftBoxAdminController', 'logout',GiftBoxAuthentification::ACCESS_LEVEL_USER);
$router->addRoute('signup', '/signup/', '\giftBoxApp\control\GiftBoxAdminController', 'signup',GiftBoxAuthentification::ACCESS_LEVEL_NONE);
$router->addRoute('checksignup', '/checksignup/', '\giftBoxApp\control\GiftBoxAdminController', 'checksignup',GiftBoxAuthentification::ACCESS_LEVEL_NONE);
$router->addRoute('coffrets', '/coffrets/', '\giftBoxApp\control\GiftBoxController', 'viewCoffrets',GiftBoxAuthentification::ACCESS_LEVEL_NONE);
$router->addRoute('coffret', '/coffretDetail/', '\giftBoxApp\control\GiftBoxController', 'viewCoffretDetail',GiftBoxAuthentification::ACCESS_LEVEL_NONE);



$router->setDefaultRoute('/prestations/');

$router->run();


?>